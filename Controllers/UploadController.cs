using Microsoft.AspNetCore.Mvc;
using TestApi1.Models;

namespace TestApi1.Controllers;

[ApiController]
[Route("[controller]")]
public class UploadController : ControllerBase
{
    private readonly ILogger<UploadController> _logger;
    private readonly ThingRepository _repository;
    private readonly IConfiguration _conf;

    public UploadController(
        ILogger<UploadController> logger,
        ThingRepository repository,
        IConfiguration conf
    )
    {
        _logger = logger;
        _repository = repository;
        _conf = conf;
    }

    [HttpPost("/upload/{id}"), DisableRequestSizeLimit]
    /// <summary>
    /// Dodawanie pliku do rekordu o określonym id
    /// </summary>
    public async Task<IActionResult> Upload(string id)
    {
        // jeżeli nie ma powiązanych metadanych, to błąd 404
        var thing = _repository.Get(id);
        if (thing is null)
            return NotFound();

        // odczytaj plik z żądania HTTP
        var formCollection = await Request.ReadFormAsync();
        var file = formCollection.Files.First();

        // zapisz do folderu uploads/
        using (
            var fs = new FileStream(
                Path.Combine(_conf["UploadDir"], $"{id}.jpg"),
                FileMode.CreateNew,
                FileAccess.Write
            )
        )
            await file.CopyToAsync(fs);

        return Ok();
    }

    /// <summary>
    /// Tworzenie rekordu o określonym id
    /// id jest niewymagane w obiekcie
    /// </summary>
    [HttpPost("/create")]
    public IActionResult Create([FromBody] Thing f)
    {
        var result = _repository.Add(f);
        return CreatedAtAction(nameof(Create), result);
    }
}
