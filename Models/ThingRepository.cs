using HashidsNet;

namespace TestApi1.Models;

public class ThingRepository
{
    private List<Thing> things;
    private Hashids _hash;

    public ThingRepository(Hashids hash)
    {
        things = new List<Thing>();
        _hash = hash;
    }

    public Thing Add(Thing f)
    {
        if (things.Count > 0)
            f.Id = _hash.Encode(things.Max(x => _hash.Decode(x.Id)[0] + 1));
        else
            f.Id = _hash.Encode(1);

        things.Add(f);

        return f;
    }

    public Thing Get(string id)
    {
        return things.FirstOrDefault(x => x.Id == id);
    }
}
