using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.FileProviders;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<TestApi1.Models.ThingRepository>();
builder.Services.AddSingleton<HashidsNet.Hashids>(new HashidsNet.Hashids(salt: "some-secret-salt"));

// builder.Services.Configure<FormOptions>(o =>
// {
//     o.ValueLengthLimit = int.MaxValue;
//     o.MultipartBodyLengthLimit = int.MaxValue;
//     o.MemoryBufferThreshold = int.MaxValue;
// });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseStaticFiles();
app.UseStaticFiles(
    new StaticFileOptions()
    {
        // z tego katalogu będą brane pliki pod /uploads/nazwapliku.jpg
        FileProvider = new PhysicalFileProvider(app.Configuration["UploadDir"]),
        RequestPath = new PathString("/uploads")
    }
);

app.UseAuthorization();

app.MapControllers();

app.Run();
