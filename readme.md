# Kolejność operacji:

Najpierw metadatane, potem właściwy obrazek.

## POST na https://localhost:7169/create

```
{
	"name": "test",
	"description": "test"
}
```

Zwróci obiekt z dodanym identyfikatorem:

```
{
  "id": "YQ",
  "name": "test",
  "description": "test"
}
```

## POST na https://localhost:7169/upload/{id}

Typu form-data z kluczem "file" i plikiem.

Zwróci 200, bez treści.

## GET na https://localhost:7169/uploads/{id}.jpg

Zwróci plik.

## Zwróć uwagę na

* W appsettings.json jest bezwzględna ścieżka do folderu, który przechowuje
  uploady,
* Program.cs konfiguruje PhysicalFileProvider do serwowania uploadów,
* UploadController.cs robi wszystko,
* Tutaj jest używany hashids.net do generowania id, aby nie były numerkami tylko
  łańcuchami znaków, ale to nieistotny dodatek.
